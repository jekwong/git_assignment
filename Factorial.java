//Code to calculate factorial of a number
//program that calculates factorials
import java.util.Scanner;
//The main class 
// Factorial class
class Factorial
{
   public static void main(String args[])
   {
      int n, c, fact = 1;
 
      System.out.println("Enter an integer to calculate it's factorial");
      //Comment to conflict taking inputs
      //prompt
      Scanner in = new Scanner(System.in);
 
      n = in.nextInt();
 //Test end case
 //test case if integer is negative
      if ( n < 0 )
         System.out.println("Number should be non-negative.");
      else
      {
//Actual logic for calculating factorial
//calculating factorials
         for ( c = 1 ; c <= n ; c++ )
            fact = fact*c;
 
         System.out.println("Factorial of "+n+" is = "+fact);
      }
   }
}
